from django.db import models

# Create your models here.
class Topic (models.Model):
	top_name = models.CharField(max_length=264,unique=True)

	def __str__(self):
		return self.top_name


class Webpage(models.Model):
	topic = models.ForeignKey(Topic)
	name = models.CharField(max_length = 264, unique = True)
	url = models.URLField(unique = True)

	def __str__(self):
		return self.name
		

class AccessRecord(models.Model):
	name = models.ForeignKey(Webpage)
	date = models.DateField()

	def __str__(self):
		return str(self.date)


class StudentInfo(models.Model):
	stu_id = models.BigIntegerField(unique=True)
	stu_name = models.CharField(max_length=500)
	stu_roll = models.BigIntegerField(null=False)
	stu_class = models.CharField(max_length=500,null=False)

	def save_stu_info(self):
		self.save()