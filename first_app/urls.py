from django.conf.urls import url
from first_app import views


urlpatterns = [
	url(r'^$',views.index,name='index'),   # http://127.0.0.1:8000/first_app/
	url(r'^abc',views.index,name='index'), # http://127.0.0.1:8000/first_app/abc
	url(r'^database', views.show_data,name='show_data'),# http://127.0.0.1:8000/first_app/show_data
	url(r'^show_form', views.form_render,name='show_form'),# http://127.0.0.1:8000/first_app/show_form
]
print('this is FIRST APP URL.py FIle')
