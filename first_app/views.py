from django.shortcuts import render
from django.http import HttpResponse
from .import form
from first_app.models import Topic, Webpage, AccessRecord
# Create your views here.
print("from First_app views.py ")


def index(request):

    my_dict = {'insert_from_python':"https://stackoverflow.com/questions/28641210/turn-url-into-html-link",
    			'ajaira':"faltu habijabi"}

    baseDict ={1:['b11',100,0.5],
               2:['b22',200,0.009]}

    queryDict ={1:[['q11', 99, 88],['q112', 2299, 2288]],
                2:[['q22', 22, 22],['q112', 222, 222]]}
    queryDict2 ={1:[['q5555', 99, 88],['q555', 2299, 2288]],
                2:[['q666', 22, 22],['q666', 222, 222]]}



    allDict = {'baseDict':baseDict,'queryDict':queryDict}
    return render(request, 'first_app/index.html',  {'baseDict':baseDict,'queryDict':queryDict,'queryDict2':queryDict2})


def show_data(request):
    dataFromAccessRecord = AccessRecord.objects.order_by('date')
    dict_data = {'AccessRecord': dataFromAccessRecord}
    return render(request, 'first_app/ShowData.html',context = dict_data)


def form_render(request):
    formDct = form.FormStructure()
    print('\n\nform')
    if request.method == 'POST':
        print('post called....')
        formDct = form.FormStructure(request.POST)
        if formDct.is_valid():
            print('valid!')
            print(formDct.cleaned_data['name'])
    return render(request,'first_app/form_page.html', {'formDct':formDct} )
